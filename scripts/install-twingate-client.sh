#!/bin/bash

echo "deb [trusted=yes] https://packages.twingate.com/apt/ /" | tee /etc/apt/sources.list.d/twingate.list
apt update -yq
apt install -yq twingate  # or twingate-latest
# After installation, configure the client by running: sudo twingate setup