#!/bin/bash

# export AWS_PROFILE="710memorial"
# export AWS_DEFAULT_REGION="us-east-1"
# export TWINGATE_ACCESS_TOKEN="access_token"
# export TWINGATE_REFRESH_TOKEN="refresh_token"

IFS= USER_DATA=$(cat <<EOF
#! /bin/bash
[ ! -d "/etc/twingate" ] && sudo mkdir /etc/twingate
echo TWINGATE_URL="https://710memorial.twingate.com" > /etc/twingate/connector.conf
echo TWINGATE_ACCESS_TOKEN="$TWINGATE_ACCESS_TOKEN" >> /etc/twingate/connector.conf
echo TWINGATE_REFRESH_TOKEN="$TWINGATE_REFRESH_TOKEN" >> /etc/twingate/connector.conf
sudo systemctl enable twingate-connector
sudo systemctl start twingate-connector
EOF
)

export TWINGATE_AMI=$(aws ec2 describe-images --owners 617935088040 --filters "Name=name,Values=twingate/images/hvm-ssd/twingate-amd64-*" --query 'sort_by(Images, &CreationDate)[].ImageId' | fgrep ami | cut -d '"' -f2 | tail -1)
aws ec2 run-instances --image-id $TWINGATE_AMI --user-data $USER_DATA --count 1 --instance-type t3a.micro --subnet-id subnet-053f6b68d01b5a974

# add connector ec2 instance to security groups of the resource you want to connect to